// variatic function (arguments is varable length)
// pass in array like obj and want to push 42 on the front
function foo(arguments) {
    let args = [].slice.call(arguments);
    args.unshift(42); // push to front
    bar.apply(null, args); // pass values as individual args (not array)
}

// rest (gather) / spread
function foo( ...args ) {
    // imperitive form
    // args.unshift(42);
    // bar(...args);

    //declaritive form
    bar(42,...args);
}


// ****************************
// apply(), slice(), concat() 
// value of x, y - add 0 to front, add 6 to end
let x = [1, 2, 3];
let y = [4, 5];
let z = [0].concat(x, y, [6]);

let x = [1, 2, 3];
let y = [4, 5];
let z = [0, ...x, ...y, 6];
// **************************** 