function foo() {
    return {a: 1, b: 2, c: 3};
}

// let tmp = foo();
// let a = tmp.a;
// let b = tmp.b;
// let c = tmp.c;


let { a, b, c} = foo();

// if name and property diff... 
// get properties: {a: ,  x: ,  c: }
let { 
    a = 73, 
    b: x = 42, 
    c
} = foo() || {};
