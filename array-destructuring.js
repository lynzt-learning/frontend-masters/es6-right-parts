// function foo() {
//     return [1, 2, 3];
// }

// let tmp = foo();
// let a = tmp[0];
// let b = tmp[1];
// let c = tmp[2];

function foo() {
    return [1, 2, 3];
}

let [a, b, c] = foo(); // pattern matching(ish)




// ******************************
// pattern match - can receive with more or less params...
// will throw away or ignore those that don't match
function foo() {
    return [1];
}

var [
    a,
    b = 42, // add default value
    c,
] = foo();


// ******************************
// if returns array not returned, will throw err...
// can default to array object to prevent type err
function foo() {
    return null;
}

var [
    a,
    b = 42, // add default value
    c,
] = foo() || [];
