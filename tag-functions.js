function foo(strings, ...values) {
    let str = '';
    for (let i = 0; i < strings.length; i++) {
        if (i > 0) str+= values[i-1];
        str += strings[i];
    }
    return str;
}

var name = 'harry';
var pet = 'owl';
var school = 'hogwarts';


// var msg = `${name} goes to ${school} and has an ${pet}`;
// add foo - calls fcn...
var msg = foo`${name} goes to ${school} and has an ${pet}.`;
