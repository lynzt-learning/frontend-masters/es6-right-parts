// swap values...
let x = 1;
let y = 2;
let tmp = x;
x = y;
y = tmp;

[x, y] = [y, x]; 

// **********
var a = [1, 2, 3];

// array dumping... 
[ , , ...a] = [0, ...a, 4]; // a => [2, 3, 4]   


// *************************
function foo() {
    return [1, 2, 3, 4];
}

var a, b;
var x = [a, b] = foo();
// a => 1, b => 2, x = [1, 2, 3, 4]


