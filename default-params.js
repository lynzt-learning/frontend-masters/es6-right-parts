// default arguments
function foo(x) {
    // x = x || 42; // bad - can't pass in 0 or null (falsy values)
    x = x !== undefined ? x : 42;
}

// es6
function foo(x = 42) {

}

// can default using any expression... (lazy expression)
function uniqueId() {
    return generateId();
}

function foo(x = uniqueId()) {
    // blah
}
